#!/bin/sh

# Windows 32-bit build script.
# $BUILD_DIR, $CACHE_DIR, $TEMP_DIR, $LOVE_VERSION, $ICON_FILE,
# $LOVE_FILE and $GAME_NAME must be defined.
# Remember to update checksum if $LOVE_VERSION is changed !

HERE=$( cd $( dirname "${BASH_SOURCE[0]}" ) && pwd )
source $HERE/toolkit.sh

LINUX_BUILD_DIR="$BUILD_DIR/linux"
LINUX_LOVE_APPIMAGE_64_LINK="https://bitbucket.org/rude/love/downloads/love-$LOVE_VERSION-x86_64.AppImage"
LINUX_LOVE_APPIMAGE_64_HASH="6485a7b2b657600f6cd032d4e5b6875bb87a54f6c11be6db8e5ebeba4fd0897f"
LINUX_LOVE_APPIMAGE_32_LINK="https://bitbucket.org/rude/love/downloads/love-$LOVE_VERSION-i686.AppImage"
LINUX_LOVE_APPIMAGE_32_HASH="f7f47defd01056093af91f5b00254da0b160388d7b89d0f4f5b424c3dfb84a01"
LINUX_APPIMAGEKIT_LINK="https://github.com/AppImage/AppImageKit/releases/download/10/appimagetool-x86_64.AppImage"
LINUX_APPIMAGEKIT_HASH="2f8a62f8ad1a4ad9608132bc467d6cc3f2c948ec5697a57b983e2824e98aeb0f"
LINUX_APPIMAGEKIT="$CACHE_DIR/appimagetool.AppImage"
LINUX_APPDIR_64="$TEMP_DIR/$GAME_NAME-x86_64.AppImage"
LINUX_APPDIR_32="$TEMP_DIR/$GAME_NAME-i686.AppImage"
LINUX_APPRUN_64="$CACHE_DIR/AppRun-x86_64"
LINUX_APPRUN_64_LINK="https://github.com/AppImage/AppImageKit/releases/download/10/AppRun-x86_64"
LINUX_APPRUN_64_HASH="498525c25b6730b02bd5a05f695fbe7ebec6788afb948b8e34090d0452f09839"
LINUX_APPRUN_32="$CACHE_DIR/AppRun-i686"
LINUX_APPRUN_32_LINK="https://github.com/AppImage/AppImageKit/releases/download/10/AppRun-i686"
LINUX_APPRUN_32_HASH="32d807a8774b85b88c12e0f0da3519173af3e7c1db150dee1b21f036aaf7d024"
LINUX_APPDESKTOP="
[Desktop Entry]
Name=$GAME_NAME
Exec=$GAME_NAME
Icon=icon
Type=Application
Categories=Game;
"

# Building linux
print_header "Building Linux"

get_file "AppImageKit" "$LINUX_APPIMAGEKIT" "$LINUX_APPIMAGEKIT_LINK" "$LINUX_APPIMAGEKIT_HASH"

LOVE2D_APPIMAGE_64="love2d_x86_64.AppImage"
LOVE2D_APPIMAGE_32="love2d_i686.AppImage"
get_file "i686 Love2d AppImage" "$CACHE_DIR/$LOVE2D_APPIMAGE_32" "$LINUX_LOVE_APPIMAGE_32_LINK" "$LINUX_LOVE_APPIMAGE_32_HASH"
get_file "x86_64 Love2d AppImage" "$CACHE_DIR/$LOVE2D_APPIMAGE_64" "$LINUX_LOVE_APPIMAGE_64_LINK" "$LINUX_LOVE_APPIMAGE_64_HASH"

print_step "Creating AppDirs for the game AppImage"
do_step 'rm -rf $LINUX_APPDIR_{32,64} &&
    chmod +x "$CACHE_DIR/$LOVE2D_APPIMAGE_32" &&
    chmod +x "$CACHE_DIR/$LOVE2D_APPIMAGE_64" &&
    (cd "$CACHE_DIR" && "./$LOVE2D_APPIMAGE_32" --appimage-extract) &&
    mv -T "$CACHE_DIR/squashfs-root" "$LINUX_APPDIR_32" &&
    (cd "$CACHE_DIR" && "./$LOVE2D_APPIMAGE_64" --appimage-extract) &&
    mv -T "$CACHE_DIR/squashfs-root" "$LINUX_APPDIR_64"'

print_step "Fusing game executable"
do_step 'cat $LINUX_APPDIR_32/usr/bin/love "$LOVE_FILE" > $LINUX_APPDIR_32/usr/bin/$GAME_NAME &&
    cat $LINUX_APPDIR_64/usr/bin/love "$LOVE_FILE" > $LINUX_APPDIR_64/usr/bin/$GAME_NAME &&
    chmod a+x $LINUX_APPDIR_{32,64}/usr/bin/$GAME_NAME'

print_step "Providing icon and desktop files"
do_step 'echo "$LINUX_APPDESKTOP" > $LINUX_APPDIR_32/$GAME_NAME.desktop &&
    echo "$LINUX_APPDESKTOP" > $LINUX_APPDIR_64/$GAME_NAME.desktop &&
    cp $ICON_FILE $LINUX_APPDIR_32/icon.${ICON_FILE#*.} &&
    cp $ICON_FILE $LINUX_APPDIR_64/icon.${ICON_FILE#*.}'

get_file "i686 AppRun file" "$LINUX_APPRUN_32" "$LINUX_APPRUN_32_LINK" "$LINUX_APPRUN_32_HASH"
get_file "x86_64 AppRun file" "$LINUX_APPRUN_64" "$LINUX_APPRUN_64_LINK" "$LINUX_APPRUN_64_HASH"

print_step "Changing AppRun files"
APPRUN_32="$LINUX_APPDIR_32/AppRun"
APPRUN_64="$LINUX_APPDIR_64/AppRun"
do_step 'rm -f $APPRUN_{32,64} &&
    cp "$LINUX_APPRUN_32" "$APPRUN_32" &&
    cp "$LINUX_APPRUN_64" "$APPRUN_64" &&
    chmod +x $APPRUN_{32,64}'

print_step "Building AppImage of the game"
do_step 'mkdir -p "$LINUX_BUILD_DIR" &&
    chmod a+x "$LINUX_APPIMAGEKIT" &&
    ( cd "$TEMP_DIR" && "$LINUX_APPIMAGEKIT" --appimage-extract ) &&
    ( ARCH=x86_64 $TEMP_DIR/squashfs-root/AppRun "$LINUX_APPDIR_64" "$LINUX_BUILD_DIR/$GAME_NAME-x86_64.AppImage" ) &&
    ( ARCH=i686 $TEMP_DIR/squashfs-root/AppRun "$LINUX_APPDIR_32" "$LINUX_BUILD_DIR/$GAME_NAME-i686.AppImage" )'
