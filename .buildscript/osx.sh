#!/bin/sh

# OS X build script.
# $BUILD_DIR, $CACHE_DIR, $TEMP_DIR, $LOVE_VERSION, $LOVE_FILE,
# $GAME_PROVIDER and $GAME_NAME must be defined.
# Remember to update checksum if $LOVE_VERSION is changed !

HERE=$( cd $( dirname "${BASH_SOURCE[0]}" ) && pwd )
source $HERE/toolkit.sh

OSX_BUILD_DIR="$BUILD_DIR/macos"
OSX_ZIPPED_FILE="$CACHE_DIR/macos_exe.zip"
OSX_ZIPPED_FILE_LINK="https://bitbucket.org/rude/love/downloads/love-$LOVE_VERSION-macos.zip"
OSX_ZIPPED_FILE_HASH="770286edc8cdab6abb7a522d05bb488ed2433dc5a76f6aca7b2ed54d7c5d4dd8"
OSX_LOVE_DIRNAME="love.app"
OSX_APP_NAME="$GAME_NAME.app"
OSX_APP="$TEMP_DIR/$OSX_APP_NAME"

# Building os x
print_header "Building OS X"

get_file "OS X zipped executable file" "$OSX_ZIPPED_FILE" "$OSX_ZIPPED_FILE_LINK" "$OSX_ZIPPED_FILE_HASH"

print_step "Extracting OS X zipped executable"
do_step 'unzip -o "$OSX_ZIPPED_FILE" -d "$TEMP_DIR"'

print_step "Making OS X game executable"
do_step 'mkdir -p "$OSX_APP" &&
    cp -r "$TEMP_DIR/$OSX_LOVE_DIRNAME/." -t "$OSX_APP" &&
    cp "$LOVE_FILE" "$OSX_APP/Contents/Resources" &&
    sed -i "63s/.*/<string>$GAME_PROVIDER<\/string>/" "$OSX_APP/Contents/Info.plist" &&
    sed -i "67s/.*/<string>$GAME_NAME<\/string>/" "$OSX_APP/Contents/Info.plist"'

print_step "Zipping OS X game"
do_step 'mkdir -p "$OSX_BUILD_DIR" &&
    cd "$OSX_APP/.." &&
    zip -9 -r "$GAME_NAME"_osx.zip "$OSX_APP_NAME" &&
    mv "$GAME_NAME"_osx.zip "$OSX_BUILD_DIR"'
