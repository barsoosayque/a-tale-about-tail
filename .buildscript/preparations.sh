#!/bin/sh

# Preparations for the build.
# $SOURCE_DIR, $CACHE_DIR, $TEMP_DIR and $GAME_NAME must be defined.
# Here is where $LOVE_FILE is builded.

HERE=$( cd $( dirname "${BASH_SOURCE[0]}" ) && pwd )
source $HERE/toolkit.sh

SRC_LIST="
conf.lua
enemy.lua
main.lua
menu.lua
music.lua
player.lua
stage.lua
objects.lua
stg
dat
lib/anim8/anim8.lua
lib/bump/bump.lua
lib/gamera/gamera.lua
lib/gspot/Gspot.lua
"
SRC_LIST=$(echo $SRC_LIST | tr '\n\r' ' ')

export LOVE_FILE="$TEMP_DIR/archived.love"

# Preparations
print_header "Preparations"

# Creating directories
print_step "Making temporary and build directories"
do_step 'mkdir -p "$BUILD_DIR" "$TEMP_DIR" "$CACHE_DIR"'

# Zippin' files
print_step "Make zip archive with game files"
do_step 'cd "$SOURCE_DIR" && zip -9 -r "$LOVE_FILE" $SRC_LIST'
