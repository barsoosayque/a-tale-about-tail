#!/bin/sh

# Functions for the build script

# Check hashes of the file
# Arguments: 
#   - $1: arbitrary description of the checked file
#   - $2: file path
#   - $3: hash
check_hash() {
    print_step "Checking sha256sum of the $1"
    do_step sha256sum -c --status <<<"$3 $2"
}

# Download file if it is not exist, and then check hashes
# Arguments:
#   - $1: arbitrary desctription of the checked file
#   - $2: file name
#   - $3: file link
#   - $4: file hash
get_file() {
    if [ ! -f "$2" ]; then
        print_step "Downloading $1"
        do_step wget --no-verbose "$3" -O "$2".part '&&' mv "$2".part "$2"
    fi
    check_hash "$1" "$2" "$4"
}

# Make another step to the victory !
# Arguments:
#   -$*: Command that will be executed in a fancy way
do_step() {
    CMD="$*"
    # for token in $*; do
    #     if [[ "$token" =~ ^\s*[\"]?\$[a-zA-Z0-9_]+ ]]; then
    #         CMD="$CMD $(echo $token | envsubst)"
    #     else 
    #         CMD="$CMD $token" 
    #     fi
    # done
    OUTPUT=$((eval "$CMD") 2>&1)
    EXITCODE="$?"

    if [[ $EXITCODE == 0 ]]; then
        step_done
    else
        step_error "$CMD" "$OUTPUT"
    fi
}

# Print pretty header
print_header() {
    echo -e "\e[1;35m===== $1 =====\e[0m"
}

# Echo that something is done
step_done() {
    echo -e "\e[32mDone !\e[0m"
}

# Echo that there is an error, and the quit
# Arguments:
#   - $1: Executed command
#   - $2: Error description
step_error() {
    echo -e "\e[31mError !\e[0m\nWhile executing: \e[36m$1\e[0m\nCaused by: \e[33m$2\e[0m"
    exit 1
}

# Something is going on
# Arguments:
#   - $1: Step short description
print_step() {
    echo -e -n "\e[33m->\e[0m $1.. "
}

