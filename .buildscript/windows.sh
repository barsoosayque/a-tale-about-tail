#!/bin/sh

# Windows 32-bit build script.
# $BUILD_DIR, $CACHE_DIR, $TEMP_DIR, $LOVE_VERSION, 
# $LOVE_FILE and $GAME_NAME must be defined.
# Remember to update checksum if $LOVE_VERSION is changed !

HERE=$( cd $( dirname "${BASH_SOURCE[0]}" ) && pwd )
source $HERE/toolkit.sh

WIN32_BUILD_DIR="$BUILD_DIR/win32"
WIN32_ZIPPED_FILE="$CACHE_DIR/win32_exe.zip"
WIN32_ZIPPED_FILE_LINK="https://bitbucket.org/rude/love/downloads/love-$LOVE_VERSION-win32.zip"
WIN32_ZIPPED_FILE_HASH="816fc2c11f23c0a023081172c1dcec2f1dbc8f46c17258dd795bd18ee731d2dd"
WIN32_LOVE_DIRNAME="love-$LOVE_VERSION.0-win32"
WIN32_APP="$TEMP_DIR/$GAME_NAME.win"
WIN32_DEPENDENCIES="
SDL2.dll
OpenAL32.dll
license.txt
love.dll
lua51.dll
mpg123.dll
msvcp120.dll
msvcr120.dll
"
WIN32_DEPENDENCIES=$(echo $WIN32_DEPENDENCIES | tr '\n\r' ' ')

# Building windows
print_header "Building Windows"

get_file "Windows 32-bit zipped executable file" "$WIN32_ZIPPED_FILE" "$WIN32_ZIPPED_FILE_LINK" "$WIN32_ZIPPED_FILE_HASH"

print_step "Extracting Windows 32-bit zipped executable"
do_step 'unzip -o "$WIN32_ZIPPED_FILE" -d "$TEMP_DIR"'

print_step "Making Windows 32-bit game executable"
do_step 'mkdir -p "$WIN32_APP" &&
    cat "$TEMP_DIR/$WIN32_LOVE_DIRNAME/love.exe" "$LOVE_FILE" > "$WIN32_APP/$GAME_NAME.exe" &&
    for file in $WIN32_DEPENDENCIES ; do cp -t "$WIN32_APP" "$TEMP_DIR/$WIN32_LOVE_DIRNAME/$file" ; done'

print_step "Zipping Windows 32-bit game"
do_step 'mkdir -p "$WIN32_BUILD_DIR" &&
    cd "$WIN32_APP" &&
    zip -9 -r "$GAME_NAME"_win.zip . &&
    mv "$GAME_NAME"_win.zip "$WIN32_BUILD_DIR"'
