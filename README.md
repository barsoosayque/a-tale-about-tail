# A Tale about Tail

### There is always a thief and a grumpy old man

A never-ending tale about the little mute fox, who appear to be Slipper, a thief. As always, the threat looms over the kingdom. This time, it's a lots of golden coins. Save the world or just feed your family - today it doesn't really matter. Grab your trusty green sack and become one of the famous ghostlike invisible of the world !

### About

Little stealth-like game. It's a some kind of platformer, where you accomplish tasks by stealing gold. Take coins and jewelry from the chests and jars, and bring them to "the Green Sack". When there is no gold left -- you've complete your task. The more gold your foxy is carrying, the slower it is. But watch out for grandpas !

### Screenshots

![a-tale-about-tail.png](https://static.jam.vg/raw/473/9/z/d12d.png) 

### Controls

* Arrows: movement
* Mouse: interface interaction

### For Linux user

To launch AppImage file, you need to follow these simple steps:

1. `$ chmod a+x ATaleAboutTale.AppImage`
2. `$ ./ATaleAboutTale.AppImage`

Also, if you ain't familiar with terminal, check [this](https://discourse.appimage.org/t/how-to-make-an-appimage-executable/80) out.

### Building

Before building, be sure to update git submodules:

```
$ git submodules update --recursive --init
```

To build Windows 32-bit, OS X and Linux game executables, there is `build.sh` shell script that can automatically do that.

After script finishes its work, there will be these files:

- *build/win32/ATaleAboutTale_win.zip*: Windows 32-bit zipped executable file with DLLs
- *build/macos/ATaleAboutTale_mac_zip*: OS X zipped app
- *build/linux/ATaleAboutTale-{ARCH}.AppImage*: AppImage for Linux. ARCH will be *x86_64* and *i686*.

Building dependencies: zip, unzip, wget, sha256sum.
