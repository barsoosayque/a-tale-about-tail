#!/bin/sh

# Shell script to build executables for:
# Windows 32-bit, Linux, OS X.
# Dependencies: zip, unzip, wget, sha256sum.

# Build configuration

LOVE_VERSION="11.2" # need to recalculate hashes if want to change this
GAME_NAME="ATaleAboutTail"
GAME_PROVIDER="tailtail-ld40"
ICON_FILE="icon.png"

# ===================

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SOURCE_DIR="$HERE/src"
BUILD_DIR="$HERE/build"
TEMP_DIR="$HERE/build/.temp"
CACHE_DIR="$HERE/build/.cache"

source .buildscript/toolkit.sh
source .buildscript/preparations.sh 
source .buildscript/windows.sh
source .buildscript/osx.sh
source .buildscript/linux.sh

# Clear temporary files
print_header "Clearing"
print_step "Removing temporary files.. "
do_step rm -r $TEMP_DIR
